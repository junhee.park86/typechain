const sayHi = (name: string, age: number, gender: string): string => {
  return `Hello ${name}, you are ${age}, and ${gender}.`;
}
const hi = sayHi("name", 123, "male");
console.log(hi);

export {};
